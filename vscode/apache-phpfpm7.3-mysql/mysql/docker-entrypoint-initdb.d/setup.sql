--
-- Database: fido2fa
--
create database saijyou;
use saijyou;

--
-- User: infotec
--
CREATE USER `infotec` IDENTIFIED BY 'mae32makase';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE ON `fido2fa`.* TO `infotec` identified by 'mae32makase';

